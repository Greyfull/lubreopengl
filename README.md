# Engine Skeleton

Project created for Engine subject at the career.

## Features

- ECS
- Message System
- Entity Controller
- Load Scene from XML
- Kernel
- Tasks
- Modules (Render, Update)

---
You can move the head with WASD or Arrow Keys

![MainScene](/doc/img/MainScene.png)