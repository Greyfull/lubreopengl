/*

*/

#include "PlayerController.hpp"

namespace lubre
{
	PlayerController::PlayerController()
		: Controller(), input(new Input),
		speed(0.f),
		moveUp(MessagesID::MSG_MOVE_UP),
		moveDown(MessagesID::MSG_MOVE_DOWN),
		moveRight(MessagesID::MSG_MOVE_RIGHT),
		moveLeft(MessagesID::MSG_MOVE_LEFT)
	{
	}

	PlayerController::~PlayerController()
	{
		delete input;
		speed = nullptr;
	}

	void PlayerController::initialize(Entity * entity)
	{
	}

	void PlayerController::update(Entity * entity, float deltaTime)
	{
		// seteamos la velocidad
		speed = 2.f * deltaTime;

		// si pulsamos arriba
		if (input->getKey(Key::W) || input->getKey(Key::ARROW_UP))
		{
			// a�adimos la informacion al mensaje
			moveUp[MessagesID::MSG_MOVE_UP] = speed;
			// enviamos el mensaje
			entity->getScene()->getDispatcher()->send(moveUp);
		}
		// si pulsamos abajo
		else if (input->getKey(Key::S) || input->getKey(Key::ARROW_DOWN))
		{
			moveDown[MessagesID::MSG_MOVE_DOWN] = speed;
			entity->getScene()->getDispatcher()->send(moveDown);
		}

		// si pulsamos derecha
		if (input->getKey(Key::D) || input->getKey(Key::ARROW_RIGHT))
		{
			moveRight[MessagesID::MSG_MOVE_RIGHT] = speed;
			entity->getScene()->getDispatcher()->send(moveRight);
		}
		// si pulsamos izquierda
		else if (input->getKey(Key::A) || input->getKey(Key::ARROW_LEFT))
		{
			moveLeft[MessagesID::MSG_MOVE_LEFT] = speed;
			entity->getScene()->getDispatcher()->send(moveLeft);
		}
	}

	void PlayerController::finalize(Entity * entity)
	{
	}
}