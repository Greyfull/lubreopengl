
#include "RenderTask.hpp"
#include "Scene.hpp"

namespace lubre
{
	RenderTask::RenderTask
	(
		Scene * scene,
		RenderNode * renderNode,
		size_t priority,
		bool finished
	)
		: 
		Task
		(
			scene, 
			priority, 
			finished
		),
		renderNode(renderNode)
	{
	}

	void RenderTask::initialize()
	{
		for (auto & comp : components)
		{
			comp->initialize();	
		}
	}

	void RenderTask::run(float deltaTime)
	{
		// llamar a clear
		scene->getWindow()->clearDisplay();

		// renderizamos 
		renderNode->render();

		// llamar a swap buffers
		scene->getWindow()->swapBuffers();
	}

	void RenderTask::finalize()
	{
		for (auto & comp : components)
		{
			comp->finalize();
		}
	}
}