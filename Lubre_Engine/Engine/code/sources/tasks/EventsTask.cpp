
extern "C"
{
	#include <SDL_events.h>
}

#include "EventsTask.hpp"
#include "Scene.hpp"

namespace lubre
{
	EventsTask::EventsTask(Scene * scene, int priority, bool finished)
		: Task(scene, priority, finished)
	{
	}

	void EventsTask::run(float deltaTime)
	{		
		SDL_Event event;

		// mientras haya eventos
		while (scene->getWindow()->pollEvents(event))
		{
			// si los eventos son de ventana
			if (event.type == SDL_WINDOWEVENT)
			{
				switch (event.window.event)
				{
					// si reescalamos la ventana
					case SDL_WINDOWEVENT_RESIZED:
							scene->getWindow()->setSize(event.window.data1, event.window.data2);
						break;
					// si cerramos la ventana
					case SDL_WINDOWEVENT_CLOSE:
							scene->getKernel()->stop();
						break;
				}
			}
		}
	}

	void EventsTask::initialize()
	{
	}

	void EventsTask::finalize()
	{
	}
}