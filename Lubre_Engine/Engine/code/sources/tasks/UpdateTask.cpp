/*
	Author: David Rogel Pernas
	Date: 1/26/2018
	Info:
*/

#include "UpdateTask.hpp"

namespace lubre
{
	UpdateTask::UpdateTask(Scene * scene, size_t priority, bool finished)
		: Task(scene, priority, finished)
	{
	}

	void UpdateTask::initialize()
	{
		for (auto & comp : components)
		{
			comp->initialize();
		}
	}

	void UpdateTask::run(float deltaTime)
	{
		for (auto & comp : components)
		{
			comp->run(deltaTime);
		}
	}	

	void UpdateTask::finalize()
	{
		for (auto & comp : components)
		{
			comp->finalize();
		}
	}
}