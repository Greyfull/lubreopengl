/*
	Author: David Rogel Pernas
	Date: 12/28/2017
	Info:
*/

#include <cassert>

extern "C"
{
	#include <SDL_video.h>
	#include <SDL_events.h>
}

#include "Window.hpp"

#include <glbinding\Binding.h>
#include <glbinding\gl\gl.h>

using namespace glbinding;
using namespace gl;

namespace lubre
{
	const int Window::opengl = SDL_WINDOW_OPENGL;
	const int Window::show = SDL_WINDOW_SHOWN;
	const int Window::hide = SDL_WINDOW_HIDDEN;
	const int Window::borderless = SDL_WINDOW_BORDERLESS;
	const int Window::resizable = SDL_WINDOW_RESIZABLE;
	const int Window::minimized = SDL_WINDOW_MINIMIZED;
	const int Window::maximazed = SDL_WINDOW_MAXIMIZED;
	const int Window::fullscreen = SDL_WINDOW_FULLSCREEN;
	const int Window::fullscreen_desktop = SDL_WINDOW_FULLSCREEN_DESKTOP;
	
	Window::Window(const char* title, int width, int height, int flags)
		: screenWidth(width), screenHeight(height)
	{
		// inicializamos el subsistema de video de SDL
		assert(!SDL_VideoInit(nullptr));

		// no precargamos ninguna libreria de opengl
		assert(!SDL_GL_LoadLibrary(nullptr));

		// seteamos los atributos de opengl

		// activamos los graficos acelerados
		assert(!SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1));
		// ponemos la version de opengl a la 3.3
		assert(!SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3));
		assert(!SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3));

		// activamos el doble buffer
		assert(!SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1));
		
		// ponemos un buffer de pixeles de 8 por cada componente
		assert(!SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8));
		assert(!SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8));
		assert(!SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8));
		// alfa es 0 por defecto
		
		// aplicamos el tama�o total de buffer
		assert(!SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24));

		// creamos la ventana centrada y preparada para trabajar con opengl
		window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, flags | SDL_WINDOW_OPENGL);
		assert(window);

		// creamos un contexto de opengl
		SDL_GLContext context = SDL_GL_CreateContext(window);
		assert(context);
		
		// inicializamos la libreria glbinding
		Binding::initialize(false);
	}

	// lo mismo que el otro constructor solo que este se le pasa una posicion concreta a la ventana
	Window::Window(const char * title, int x, int y, int width, int height, int flags)
	{
		assert(!SDL_VideoInit(nullptr));

		assert(!SDL_GL_LoadLibrary(nullptr));

		assert(!SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1));
		assert(!SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3));
		assert(!SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3));

		assert(!SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1));

		assert(!SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8));
		assert(!SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8));
		assert(!SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8));

		assert(!SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24));

		window = SDL_CreateWindow(title, x, y, width, height, flags | SDL_WINDOW_OPENGL);
		assert(window);

		SDL_GLContext context = SDL_GL_CreateContext(window);
		assert(context);

		Binding::initialize(false);
	}

	Window::~Window()
	{
		SDL_DestroyWindow(window);
	}

	int Window::getWindowID()
	{
		return SDL_GetWindowID(window);
	}

	void Window::setTitle(const char * nTitle)
	{
		SDL_SetWindowTitle(window, nTitle);
	}

	void Window::setSize(int nWidth, int nHeight)
	{
		// guardamos el tama�o de la ventana
		screenWidth = nWidth;
		screenHeight = nHeight;

		// se lo aplicamos a la ventana
		SDL_SetWindowSize(window, nWidth, nHeight);

		// refrescamos el viewport de opengl
		glViewport(0, 0, nWidth, nHeight);
	}

	int Window::pollEvents(SDL_Event & e)
	{
		return SDL_PollEvent(&e);
	}
	
	void Window::setVSync(bool value)
	{
		assert(!SDL_GL_SetSwapInterval(value));
	}

	void Window::swapBuffers()
	{
		SDL_GL_SwapWindow(window);
	}

	void Window::clearDisplay(float red, float green, float blue, float alpha)
	{
		glClearColor(red, green, blue, alpha);
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	}
}