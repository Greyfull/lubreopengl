/*
	Author: David Rogel Pernas
	Date: 2/2/2018
	Info:
*/

#include "LightComponent.hpp"
#include "TransformComponent.hpp"
#include "Entity.hpp"

namespace lubre
{
	LightComponent::LightComponent(Entity * entity) 
		: Component(entity) ,light(new Light)
	{
	}

	void LightComponent::changeLightColor(float red, float green, float blue)
	{
		// aplicamos el color que se le pasa
		light->set_color(glm::vec3(red, green, blue));
	}

	void LightComponent::initialize()
	{
		// buscamos el componente Transform
		TransformComponent * tr = dynamic_cast<TransformComponent*>(parent->getComponentByName("transform").get());

		// le pasamos el nodo que debe mover
		tr->setComponentTrasnform(light);
		// aplicamos la posicion rotacion y escala base de la luz
		tr->applyPositionRotationAndScale();
	}

	void LightComponent::run(float deltaTime)
	{
	}

	void LightComponent::finalize()
	{
	}

	bool LightComponent::parse(xmlNode * node)
	{
		return true;
	}
}