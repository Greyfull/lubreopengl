
/*
	Author: David Rogel Pernas
	Date: 2/4/2018
*/

#include "PlayerControlComponent.hpp"

namespace lubre
{
	PlayerControlComponent::PlayerControlComponent(Entity * entity, Controller * control)
		: ControlComponent(entity, control)
	{
		// a�ades los ids del Listener al Dispatcher
		entity->getScene()->getDispatcher()->addListener(MessagesID::MSG_MOVE_UP, *this);
		entity->getScene()->getDispatcher()->addListener(MessagesID::MSG_MOVE_DOWN, *this);
		entity->getScene()->getDispatcher()->addListener(MessagesID::MSG_MOVE_RIGHT, *this);
		entity->getScene()->getDispatcher()->addListener(MessagesID::MSG_MOVE_LEFT, *this);
	}

	PlayerControlComponent::~PlayerControlComponent()
	{
		// quitas el Listener del Dispatcher
		parent->getScene()->getDispatcher()->removeListener(*this);
	}

	void PlayerControlComponent::handle(Message & mess)
	{
		// Cogemos el TransformComponent de la entidad
		TransformComponent * tr = dynamic_cast<TransformComponent*>(parent->getComponentByName("transform").get());

		// segun el mensaje ...
		switch (mess)
		{
			// si se mueve arriba
			case MSG_MOVE_UP:
			{
				// obtenemos el id del mensaje
				size_t targetId = mess;
			
				// si el objeto no se sale por arriba se mueve
				if (tr->getPosition().y < 7.f)
				{
					// llamamos al metodo move 
					// y le pasamos la direcci�n de movimiento * por el contenido del mensaje (que es: velocidad * deltaTime)
					tr->move(glm::vec3(0.f, 1.f, 0.f) * mess[targetId].asFloat());
				}
			}
			break;

			// si se mueve abajo
			case MSG_MOVE_DOWN:
			{
				size_t targetId = mess;
			
				if (tr->getPosition().y > -6.f)
				{
					tr->move(glm::vec3(0.f, -1.f, 0.f) * mess[targetId].asFloat());
				}
			}
			break;

			// si se mueve la derecha
			case MSG_MOVE_RIGHT:
			{
				size_t targetId = mess;
			
				if (tr->getPosition().x < 7.f)
				{
					tr->move(glm::vec3(1.f, 0.f, 0.f) * mess[targetId].asFloat());
				}
			}
			break;

			// si se mueve a la izquierda
			case MSG_MOVE_LEFT:
			{
				size_t targetId = mess;
			
				if (tr->getPosition().x > -7.f)
				{
					tr->move(glm::vec3(-1.f, 0.f, 0.f) * mess[targetId].asFloat());
				}
			}
			break;
		}
	}
}