/*
	Author: David Rogel Pernas
	Date: 1/26/2018
	Info: 
*/

#include <string>
#include <vector>

#include "TransformComponent.hpp"
#include "Entity.hpp"

namespace lubre
{
	using std::vector;

	typedef std::string String;

	TransformComponent::TransformComponent(Entity * entity)
		: Component(entity), myTransform(nullptr), position(glm::vec3()),
		rotationX(0), rotationY(0), rotationZ(0), scale(0)
	{
	}

	void TransformComponent::applyPositionRotationAndScale()
	{
		// aplicamos la posicion
		myTransform->translate(position);
		// aplicamos la rotacion
		myTransform->rotate_around_x(glm::radians(rotationX));
		myTransform->rotate_around_y(glm::radians(rotationY));
		myTransform->rotate_around_z(glm::radians(rotationZ));
		// aplicamos la escala
		myTransform->scale(scale);
	}

	void TransformComponent::initialize()
	{
	}

	void TransformComponent::run(float deltaTime)
	{
	}

	void TransformComponent::finalize()
	{
	}
	
	bool TransformComponent::parse(xmlNode * node)
	{
		// la informacion que hay dentro de cada <tag> del "transform"
		String value;

		for
		(
			xmlNode * dataNode = node->first_node();
			dataNode;
			dataNode = dataNode->next_sibling()
		)
		{
			value = dataNode->value();

			if (String(dataNode->name()) == "position")
			{
				// parseamos la posicion

				// se optiene la cantidad de comas que hay
				size_t comaTimes = std::count(value.begin(), value.end(), ',');
				// donde va a estar la siguiente coma
				size_t comaPos = 0;
				// substring para extraer los datos
				String subString;

				// vector para guardar los datos
				vector<float> nums;

				for (size_t i = 0; i < comaTimes + 1; ++i)
				{
					// buscamos la posicion de la coma
					comaPos = value.find(',');
					// extraemos el trozo de texto que necesitamos
					subString = value.substr(0, comaPos);
					// transformamos el string a float
					nums.push_back(std::stof(subString));
					// borramos el trozo de texto que ya no necesitamos
					value.erase(0, comaPos + 1);
				}

				position.x = nums[0];
				position.y = nums[1];
				position.z = nums[2];
			}
			else if (String(dataNode->name()) == "rotation")
			{
				// el mismo metodo que para la posicion

				size_t comaTimes = std::count(value.begin(), value.end(), ',');
				size_t comaPos = 0;
				String subString;

				vector<float> nums;

				for (size_t i = 0; i < comaTimes + 1; ++i)
				{
					comaPos = value.find(',');
					subString = value.substr(0, comaPos);
					nums.push_back(std::stof(subString));
					value.erase(0, comaPos + 1);
				}

				rotationX = nums[0];
				rotationY = nums[1];
				rotationZ = nums[2];
			}
			else if (String(dataNode->name()) == "scale")
			{
				scale = std::stof(value);
			}
		}

		return true;
	}
}