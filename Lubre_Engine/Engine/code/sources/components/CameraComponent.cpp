/*
	Author: David Rogel Pernas
	Date: 1/27/2018
	Info: 
*/

#include "CameraComponent.hpp"
#include "TransformComponent.hpp"
#include "Entity.hpp"

namespace lubre
{

	CameraComponent::CameraComponent
	(
		Entity * entity,
		float fov,
		float nearPlane,
		float farPlane,
		float aspectRatio
	)
		: Component(entity),
		camera
		(
			new glt::Camera
			(
				fov,
				nearPlane,
				farPlane,
				aspectRatio
			)
		)
	{
	}

	void CameraComponent::initialize()
	{
		// buscamos el componente Transform
		TransformComponent * tr = dynamic_cast<TransformComponent*>(parent->getComponentByName("transform").get());

		// le pasamos el nodo que debe mover
		tr->setComponentTrasnform(camera);
		// aplicamos la posicion rotacion y escala base de la camara
		tr->applyPositionRotationAndScale();	
	}

	void CameraComponent::run(float deltaTime)
	{
	}

	void CameraComponent::finalize()
	{
	}

	bool CameraComponent::parse(xmlNode * node)
	{
		return true;
	}
}