/*
	Author: David Rogel Pernas
	Date: 1/28/2018
	Info:
*/

#include <cassert>

#include "ModelComponent.hpp"
#include "TransformComponent.hpp"
#include "Entity.hpp"

namespace lubre
{
	ModelComponent::ModelComponent(Entity * entity)
		: Component(entity), model(nullptr)
	{
	}

	ModelComponent::ModelComponent(Entity * entity, const std::string & filePath)
		: Component(entity), model(new ModelObj(filePath))
	{
	}

	void ModelComponent::initialize()
	{
		// buscamos el componente Transform
		TransformComponent * tr = dynamic_cast<TransformComponent*>(parent->getComponentByName("transform").get());

		// le pasamos el nodo que debe mover
		tr->setComponentTrasnform(model);
		// aplicamos la posicion rotacion y escala base de la modelo
		tr->applyPositionRotationAndScale();
	}

	void ModelComponent::run(float deltaTime)
	{
	}

	void ModelComponent::finalize()
	{
	}

	bool ModelComponent::parse(xmlNode * componentData)
	{
		// ruta relativa
		String relativePath = "../../resources/";
		// nombre del archivo
		String assetFile;

		// obtenemos lo que contiene el <tag>
		assetFile = componentData->value();

		// si esta vacio se sale del metodo
		if (assetFile.empty())
		{
			assert(false);
			return false;
		}

		// asignamos el archivo a la ruta
		relativePath += assetFile;

		// creamos el modelo con la ruta
		model.reset(new ModelObj(relativePath));

		return true;
	}
}