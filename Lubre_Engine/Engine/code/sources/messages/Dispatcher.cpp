/*

*/

#include "Dispatcher.hpp"

namespace lubre
{
	void Dispatcher::addListener(size_t messageId, Listener & listener)
	{
		// a�adimos un listener al multimap
		listeners.insert(std::pair<size_t, Listener*>(messageId, &listener));
	}

	void Dispatcher::removeListener(Listener & listener)
	{
		// por cada item del multimap
		for (auto item = listeners.begin(), end = listeners.end(); item != end; ++item)
		{
			// si el listener es el que buscamos
			if (item->second == &listener)
			{
				// lo eliminamos
				listeners.erase(item);
				return;
			}
		}
	}

	void Dispatcher::send(Message & message)
	{
		// si existe el mensaje dentro del multimap
		if (listeners.count(message))
		{
			for (auto & listener : listeners)
			{
				// manejamos el mensaje
				listener.second->handle(message);
			}
		}
	}
}