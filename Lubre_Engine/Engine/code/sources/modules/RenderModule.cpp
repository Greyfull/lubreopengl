/*
	Author: David Rogel Pernas
	Date: 2/3/2018
	Info:
*/

#include "RenderModule.hpp"
#include "ModelComponent.hpp"
#include "CameraComponent.hpp"
#include "LightComponent.hpp"
#include "Entity.hpp"

using namespace rapidxml;

namespace lubre
{
	typedef std::shared_ptr<ModelComponent> SharedModelComponent;
	typedef std::shared_ptr<CameraComponent> SharedCameraComponent;
	typedef std::shared_ptr<LightComponent> SharedLightComponent;

	void RenderModule::createComponent(Entity * entity, xmlNode * componentData)
	{
		// el nombre del <tag>
		String dataTagName;
		// el nombre del atributo
		String attribName;

		// recorremos el <tag> "component" para extraer su informacion
		for
		(
			xmlNode * data = componentData->first_node();
			data;
			data = data->next_sibling()
		)
		{
			// si es del tipo correcto
			if (data->type() == node_element)
			{
				// cogemos su nombre
				dataTagName = data->name();

				if (dataTagName.empty())
				{
					assert(false);
					return;
				}

				// parseamos los atributos de la data en busca de su nombre
				for 
				(
					xmlAttrib * attribData = data->first_attribute();
					attribData;
					attribData = attribData->next_attribute()
				)
				{
					// cogemos el nombre del atributo
					attribName = attribData->value();

					if (attribName.empty())
					{
						assert(false);
						return;
					}
				}

				// si el <tag> es "camera"
				if (dataTagName == "camera")
				{
					// creamos el componente
					SharedCameraComponent camera(new CameraComponent(entity));

					//a�adimos al renderNode el Modelo con su Nombre
					renderTask.addComponent(camera);
					renderTask.getRenderNode()->add(attribName, camera->getCamera());

					// a�adimos la la entidad el Componente con su Nombre
					entity->addComponent(attribName, SharedComponent(camera));
				}
				else if (dataTagName == "light")
				{
					// creamos el componente
					SharedLightComponent light(new LightComponent(entity));

					//a�adimos al renderNode el Modelo con su Nombre
					renderTask.addComponent(light);
					renderTask.getRenderNode()->add(attribName, light->getLight());

					// a�adimos la la entidad el Componente con su Nombre
					entity->addComponent(attribName, SharedComponent(light));
				}
				else if (dataTagName == "model")
				{
					// creamos el componente
					SharedModelComponent model(new ModelComponent(entity));

					// parseamos su informacion
					if (!model->parse(data))
					{
						assert(false);
						return;
					}

					//a�adimos al renderNode el Modelo con su Nombre
					renderTask.addComponent(model);
					renderTask.getRenderNode()->add(attribName, model->getModel());

					// a�adimos la la entidad el Componente con su Nombre
					entity->addComponent(attribName, SharedComponent(model));
				}
			}
		}
	}
}