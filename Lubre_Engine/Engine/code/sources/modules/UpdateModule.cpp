


#include "UpdateModule.hpp"
#include "TransformComponent.hpp"
#include "PlayerControlComponent.hpp"

#include "PlayerController.hpp"

#include "Entity.hpp"

using namespace rapidxml;

namespace lubre
{
	typedef std::shared_ptr<TransformComponent> SharedTransformComponent;

	void UpdateModule::createComponent(Entity * entity, xmlNode * componentData)
	{
		// el nombre del controller
		String controllerName;
		
		// buscamos en un principio algun controller
		for
		(
			xmlNode * dataNode = componentData->first_node();
			dataNode;
			dataNode = dataNode->next_sibling()
		)
		{
			// si el tipo es correcto
			if (dataNode->type() == node_element)
			{
				// obtenemos el nombre del controller
				controllerName = dataNode->value();

				// si es player
				if (controllerName == "player")
				{
					// creamos el componente con el playercontroller
					SharedComponent playerCC(new PlayerControlComponent(entity, new PlayerController));

					// a�adimos el component a la tarea de update
					updateTask.addComponent(SharedComponent(playerCC));

					// a�adimos el componente a la entidad
					entity->addComponent("playerController", SharedComponent(playerCC));
				}
				/*Otros controllers que pueda tener*/
			}
		}

		// creamos el transform
		SharedTransformComponent transform(new TransformComponent(entity));

		// parseamos su informacion
		if (!transform->parse(componentData))
		{
			assert(false);
			return;
		}

		// lo guardamos dentro de la updateTask
		updateTask.addComponent(transform);
		
		// lo guardamos dentro de la entidad con el nombre		
		entity->addComponent("transform", SharedComponent(transform));		
	}
}