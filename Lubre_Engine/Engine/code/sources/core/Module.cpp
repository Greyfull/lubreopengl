/*
	Author: David Rogel Pernas
	Date: 1/17/2018
	Info:
*/

#include "Module.hpp"

#include "Scene.hpp"

namespace lubre
{
	Module::Module(Scene * scene) : parent(scene) {}
	Module::~Module() { delete parent; }
}