/*
	Athor: David Rogel Pernas
	Date: 1/16/2018
	Info: 
*/

#include "ModuleFactory.hpp"

#include "RenderModule.hpp"
#include "UpdateModule.hpp"

namespace lubre
{
	ModuleFactory * ModuleFactory::instance = nullptr;
		
	ModuleFactory::ModuleFactory()
	{
		// registramos los modulos basicos
		registated("render", RenderModule::create);
		registated("transform", UpdateModule::create);		
	}	
}