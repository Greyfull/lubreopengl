/*
	Author: David Rogel Pernas
	Date: 1/26/2018
	Info:
*/

#include "Task.hpp"
#include "Scene.hpp"

namespace lubre
{
	Task::Task(Scene * scene, size_t priority, bool finished)
		: scene(scene), priority(priority), finished(finished)
	{
	}

	Task::~Task()
	{
		components.clear();
		delete scene;
	}
}