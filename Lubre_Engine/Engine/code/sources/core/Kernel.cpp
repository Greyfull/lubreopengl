/*
	Author: David Rogel Pernas
	Date: 12/31/2017
	Info:
*/

#include "Kernel.hpp"

namespace lubre
{
	Kernel::Kernel() : running(true),
		lastTime(highClock::now()),
		time(highClock::now()),
		deltaTime(time - lastTime)
	{
	}

	void Kernel::initialize()
	{
		// inicializamos todas las tareas
		for (auto & task : taskList)
		{
			task->initialize();
		}

		running = true;
	}

	void Kernel::execute()
	{
		do
		{
			// tomamo el tiempo actual
			time = highClock::now();
			
			// recorremos todas las tareas
			for
			(
				auto iterator = taskList.begin(), end = taskList.end();
				iterator != end && running;
				++iterator
			)
			{
				Task * task = *iterator;

				// ejecutamos la tarea pasandole el deltaTime
				task->run((float)deltaTime.count());

				// si la tarea es volatil
				if (task->isFinished())
				{
					// la finalizamos
					task->finalize();
					// la borramos del la lista
					taskList.erase(iterator);
				}
				// si la lista est� vacia se para el kernel y se sale del bucle
				if (taskList.empty()) { stop(); break; }
			}

			// calculamos el tiempo entre frames
			deltaTime = elapsed(time - lastTime);
			
			// registramos el tiempo del ultimo frame
			lastTime = time;
		}
		while (running);

	}

	void Kernel::finalize()
	{
		// finalizamos las tareas
		for (auto & task : taskList)
		{
			task->finalize();
		}
	}
}