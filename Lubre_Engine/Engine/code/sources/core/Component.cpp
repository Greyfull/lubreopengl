/*
	Author: David Rogel Pernas
	Date: 1/17/2018
	Info:
*/

#include "Component.hpp"

#include "Entity.hpp"

namespace lubre
{
	Component::Component(Entity * entity) : parent(entity) {}
	Component::~Component() { delete parent; }
}