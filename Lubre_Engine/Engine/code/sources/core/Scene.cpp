/*
	Author: David Rogel Pernas
	Date: 12/29/2017
	Info:
*/

#include <vector>
#include <fstream>

#include "Scene.hpp"
#include "TransformComponent.hpp"
#include "RenderModule.hpp"
#include "ModuleFactory.hpp"
#include "EventsTask.hpp"

using namespace rapidxml;

namespace lubre
{	
	Scene::Scene(const String & sceneFilePath, Window * window)
		: window(window), kernel(new Kernel), dispatcher(new Dispatcher)
	{
		// cargamos la escena
		loadScene(sceneFilePath);

		// por cada modulo ... 
		for
		(
			auto iterator = mapOfModules.begin(), end = mapOfModules.end();
			iterator != end;
			++iterator
		)
		{
			// obtenemos su tarea y la a�adimos al kernel
			kernel->addTask(*iterator->second->getTask());
		}

		// creamos la tarea de eventos
		EventsTask * eventTask = new EventsTask(this);

		// la a�adimos al kernel
		kernel->addTask(*eventTask);

		// iniciamos el kernel
		kernel->initialize();
	}

	bool Scene::loadScene(const String & sceneFilePath)
	{
		using std::fstream;

		// abrimos el fichero xml
		fstream xmlFile(sceneFilePath, fstream::in);
		
		// si lo ha abierto correctamente
		if (xmlFile.good())
		{
			// vamos a llenar un vector con el contenido del xml
			std::vector<char> xmlContent;

			bool end = false;

			while (!end)
			{
				// extrae cada caracter del archivo hasta que llega al -1 que es el final
				int character = xmlFile.get();

				if (character != -1)
				{
					xmlContent.push_back((char)character);
				}
				else
				{
					end = true;
				}
			}

			// cerramos el archivo
			xmlFile.close();

			// a�adimos un caracter nulo al final
			xmlContent.push_back(0);

			// creamos el documento de rapid xml
			xml_document<> document;

			// parseamos la informaci�n
			document.parse<0>(xmlContent.data());

			// tomamos el nodo principal
			xmlNode * root = document.first_node();

			// si el nodo es el correcto ...
			if (root && String(root->name()) == "scene")
			{				
				// parseamos la escena
				if (!parseScene(root))
				{
					assert(false);
					return false;
				}
			}

			return true;
		}
		
		assert(false);
		return false;
	}

	bool Scene::parseScene(xmlNode * sceneNode)
	{
		// recorremos todos los hijos del nodo Scene
		for 
		(
			xmlNode * child = sceneNode->first_node();
			child; 
			child = child->next_sibling()
		)
		{
			// si el tipo es correcto
			if (child->type() == node_element)
			{
				// si el nombre del <tag> es "entities"...
				if (String(child->name()) == "entities")
				{
					// parseamos las entidades
					if (!parseEntities(child))
					{
						assert(false);
						return false;
					}
				}
				else if (String(child->name()) == "config")
				{

				}
			}
		}

		// si todo va bien
		return true;
	}

	bool Scene::parseEntities(xmlNode * entitiesNode)
	{
		String name;
		
		// recorremos las entity dentro del <tag> "entities"
		for 
		(
			xmlNode * entityNode = entitiesNode->first_node();
			entityNode; 
			entityNode = entityNode->next_sibling()
		)
		{
			// si el tipo es correcto
			if (entityNode->type() == node_element)
			{
				// si el nombre del <tag> no es entidad salimos
				if (String(entityNode->name()) != "entity")
				{
					assert(false);
					return false;
				}
			}

			// buscamos y guardamos el attributo de la entidad			
			// recorremos los atributos de cada entity
			for
			(
				xmlAttrib * attribute = entityNode->first_attribute();
				attribute;
				attribute = attribute->next_attribute()
			)
			{
				// si coincide con el nombre
				if (String(attribute->name()) == "name")
				{
					// guardamos su valor
					name = attribute->value();
				}
			}

			// salimos si no encuentra
			if (name.empty())
			{
				assert(false);
				return false;
			}

			SharedEntity entity(new Entity(this));

			// recorremos los components de la entity
			for 
			(
				xmlNode * componentsNode = entityNode->first_node();
				componentsNode;
				componentsNode = componentsNode->next_sibling()
			)
			{
				// si el tipo es correcto
				if (componentsNode->type() == node_element)
				{
					// si el nombre del <tag> es "components"
					if (String(componentsNode->name()) == "components")
					{
						// parseamos los componentes
						if (!parseComponents(componentsNode, *entity))
						{
							assert(false);
							return false;
						}
					}
				}
			}
			
			// despues de parsear los components de la entity
			// a�adimos la entity al mapa de entities
			mapOfEntities[name] = entity;
		}

		// si todo va bien
		return true;
	}

	bool Scene::parseComponents(xmlNode * componentsNode, Entity & entity)
	{
		String type;

		// recorremos todos los componentes 
		for 
		(
			xmlNode * componentNode = componentsNode->first_node(); 
			componentNode; 
			componentNode = componentNode->next_sibling()
		)
		{
			// si el tipo es correcto
			if (componentNode->type() == node_element)
			{
				// Si el nombre del <tag> no es "component" salimos
				if (String(componentNode->name()) != "component")
				{
					assert(false);
					return false;
				}
			}

			// recorremos los atributos de los componentes
			for
			(
				xmlAttrib * attribute = componentNode->first_attribute();
				attribute;
				attribute = attribute->next_attribute()
			)
			{
				if (String(attribute->name()) == "type")
				{
					type = attribute->value();
				}
			}

			if (type.empty())
			{
				assert(false);
				return false;
			}
			
			// si no existe el Modulo de "tipo" type
			if (mapOfModules.count(type) == 0)
			{
				// lo creamos con la Factoria 
				mapOfModules[type] = ModuleFactory::getInstance()->createModule(type, this);
			}
			
			// cogemos el modigo
			Module * module = mapOfModules[type].get();
						
			if (!module)
			{
				assert(false);
				return false;
			}

			// creamos el componente con el modulo
			module->createComponent(&entity, componentNode);			
		}

		return true;
	}
}