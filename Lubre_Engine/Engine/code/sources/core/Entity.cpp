/*
	Author: David Rogel Pernas
	Date: 12/28/2017
	Info:
*/

#include "Entity.hpp"

#include "Scene.hpp"

namespace lubre
{
	Entity::Entity(Scene * scene) : parent(scene) {}
	Entity::~Entity() {	delete parent; }
}