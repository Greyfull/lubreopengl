
/**
* @file Dispatcher.hpp
* @author David Rogel Pernas
* @date 21/1/2018
* @class Dispatcher
* @brief clase que envia los mensajes a sus destinatarios a traves de un listener
*/

#ifndef LUBRE_DISPATCHER_HEADER
#define LUBRE_DISPATCHER_HEADER

#include <unordered_map>
#include <map>
#include <memory>

#include "Message.hpp"

namespace lubre
{
	class Dispatcher
	{	
	public:

		/**
		* @brief estructura Listener que tiene un metodo, a heredar, que "maneja" el mensaje
		*/
		struct Listener
		{
			virtual ~Listener() {}
			virtual void handle(Message &) = 0;
		};

	private:

		/**
		* @brief multimap que guarda los listeners y sus claves
		* @param
		*/
		typedef std::unordered_multimap<size_t, Listener * > ListenerMap;

	private:

		ListenerMap listeners;

	public:

		/**
		* @brief a�ade un listener 
		* @param messageId -> id del mensaje 
		* @param listener -> listener a a�adir
		*/
		void addListener(size_t messageId, Listener & listener);
		/**
		* @brief elimina un listener
		* @param listener -> listener a borrar
		*/
		void removeListener(Listener & listener);
		/**
		* @brief busca los mensajes y envia uno a traves del handler del listener
		* @param message -> mensaje a enviar
		*/
		void send(Message & message);
	};
}

#endif