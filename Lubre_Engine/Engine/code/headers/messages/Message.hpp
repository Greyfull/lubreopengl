
/**
* @file Message.hpp
* @author David Rogel Pernas
* @date 21/1/2018
* @class Message
* @brief clase que guarda la informacion del mensaje
*/

#ifndef LUBRE_MESSAGE_HEADER
#define LUBRE_MESSAGE_HEADER

#include <unordered_map>

#include "Variant.hpp"

namespace lubre
{
	class Message
	{
	private:
		
		typedef std::string String;
		typedef std::unordered_map<size_t, Variant> mapOfParameters;

	private:

		/**
		* @brief ID del mensaje
		*/
		size_t actionId;
		/**
		* @brief mapa de parametros
		*/
		mapOfParameters parameters;

	public:

		Message(size_t id) : actionId(id) {}

	public:

		Variant & operator [] (size_t key)
		{
			return parameters[key];
		}
		
		bool operator == (const Message & other) const
		{
			return this->actionId == other.actionId;
		}

		bool operator != (const Message & other) const
		{
			return !(*this == other);
		}

		operator size_t() const
		{
			return actionId;
		}
	};
}

#endif