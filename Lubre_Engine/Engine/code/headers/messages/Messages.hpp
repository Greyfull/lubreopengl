
/**
* @file Messages.hpp
* @author David Rogel Pernas
* @date 21/1/2018
* @class Messages
* @brief Este archivo lo puede modificar todo el mundo,
* contiene los IDs de los mensajes
*/

#ifndef MESSAGESID
#define MESSAGESID

/**
* @brief Enumeracion con los IDs de los mensajes
*/
enum MessagesID
{
	MSG_MOVE_UP = 1,
	MSG_MOVE_DOWN = 2,
	MSG_MOVE_RIGHT = 3,
	MSG_MOVE_LEFT = 4
};

#endif