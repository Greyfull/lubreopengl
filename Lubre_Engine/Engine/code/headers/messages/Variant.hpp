
/**
* @file Variant.hpp
* @author David Rogel Pernas
* @date 21/1/2018
* @class Variant
* @brief clase que guarda informacion y puede pasar de ciertos tipos de info a otros
*/

#ifndef LUBRE_VARIANT_HEADER
#define LUBRE_VARIANT_HEADER

#include <string>

namespace lubre
{
	class Variant
	{
	private:

		typedef std::string String;

	private:

		enum Type
		{
			NUL, 
			BOOL, 
			INT, 
			FLOAT,  
			STRING
		};

		union Data
		{
			bool boolValue;
			int	intValue;
			float floatValue;
			String * stringValue;
		};

		Type type;
		Data data;

	public:

		Variant() : type(NUL) {}

		Variant(bool b) : type(BOOL)
		{
			data.boolValue = b;
		}

		Variant(int i) : type(INT)
		{
			data.intValue = i;
		}

		Variant(float f) : type(FLOAT)
		{
			data.floatValue = f;
		}

		Variant(const String & str) : type(STRING)
		{
			data.stringValue = new String(str);
		}

		~Variant()
		{
			clear();
		}

	public:

		void clear(Type t = NUL)
		{
			if (type == STRING) delete data.stringValue;

			type = t;
		}

		bool isNULL() const { return type == NUL; }
		bool isBOOL() const { return type == BOOL; }
		bool isINT() const { return type == INT; }
		bool isFLOAT() const { return type == FLOAT; }
		bool isSTRING() const { return type == STRING; }

		int asInt() const
		{
			switch (type)
			{
				case BOOL: return data.boolValue;

				case INT: return data.intValue;

				case FLOAT: return int(data.floatValue);

				case STRING: return std::stoi(*data.stringValue);

				default: return 0;					
			}
		}

		float asFloat() const
		{
			switch (type)
			{
				case INT: return float(data.floatValue);

				//case BOOL: return float(data.boolValue);

				case FLOAT: return data.floatValue;

				case STRING: return std::stof(*data.stringValue);

				default: return 0.0f;
			}
		}

		String asString() const
		{
			switch (type)
			{
				case INT: return std::to_string(data.intValue);

				case FLOAT: return std::to_string(data.floatValue);

				case STRING: return *data.stringValue;

				default: return String();
			}
		}

		bool asBool() const
		{
			switch (type)
			{
				case BOOL: return data.boolValue;

				case INT: return (data.intValue != 0) ? 1 : 0;

				case FLOAT: return (int(data.floatValue) != 0) ? 1 : 0;				

				default: false;
			}
		}

	public:
		
		Variant & operator = (const nullptr_t &)
		{
			clear(); return *this;
		}

		Variant & operator = (const Variant & other)
		{
			clear(other.type);
			this->data = other.data;
			return *this;
		}

		Variant & operator = (const bool b)
		{
			clear(BOOL);
			data.boolValue = b;
			return *this;
		}

		Variant & operator = (const int i)
		{
			clear(INT);
			data.intValue = i;
			return *this;
		}

		Variant & operator = (const float f)
		{
			clear(FLOAT);
			data.floatValue = f;
			return *this;
		}

		Variant & operator = (const String & f)
		{
			clear(STRING);
			*data.stringValue = f;
			return *this;
		}
	};
}

#endif
