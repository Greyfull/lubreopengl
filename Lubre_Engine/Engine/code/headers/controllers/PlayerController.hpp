
/**
* @file PlayerController.hpp
* @author David Rogel Pernas
* @date 4/2/2018
* @class PlayerController
* @brief Clase para manejar el Jugador, envia mensajes segun que tecla pulses
*/

#ifndef LUBRE_PLAYER_CONTROLLER_HEADER
#define LUBRE_PLAYER_CONTROLLER_HEADER

#include "Controller.hpp"
#include "ControlComponent.hpp"
#include "TransformComponent.hpp"

#include "Message.hpp"
#include "Messages.hpp"
#include "Scene.hpp"

#include "Input.hpp"


namespace lubre
{
	class PlayerController : public Controller
	{
	private:

		Input * input;

		Variant speed;

		/**
		* @brief mensaje de moverse arriba
		*/
		Message moveUp;
		/**
		* @brief mensaje de moverse abajo
		*/
		Message moveDown;
		/**
		* @brief mensaje de moverse a la derecha
		*/
		Message moveRight;
		/**
		* @brief mensaje de moverse a la izquierda
		*/
		Message moveLeft;

	public:

		PlayerController();
		~PlayerController();

	public:

		void initialize(Entity * entity) override;
		void update(Entity * entity, float deltaTime) override;
		void finalize(Entity * entity) override;
	};
}

#endif