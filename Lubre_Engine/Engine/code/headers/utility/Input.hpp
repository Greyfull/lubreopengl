
/**
* @file Input.hpp
* @author David Rogel Pernas
* @date 9/1/2018
* @class Input
* @brief Clase que encapsula el teclado de SDL
*/

#ifndef LUBRE_INPUT_HEADER
#define LUBRE_INPUT_HEADER

#include <cstdint>

namespace lubre
{
	struct Key
	{
		enum ScanCode
		{
			A,
			B,
			C,
			D,
			E,
			F,
			G,
			H,
			I,
			J,
			K,
			L,
			M,
			N,
			O,
			P,
			Q,
			R,
			S,
			T,
			U,
			V,
			W,
			X,
			Y,
			Z,

			NUM_0,
			NUM_1,
			NUM_2,
			NUM_3,
			NUM_4,
			NUM_5,
			NUM_6,
			NUM_7,
			NUM_8,
			NUM_9,

			ARROW_LEFT,
			ARROW_RIGHT,
			ARROW_UP,
			ARROW_DOWN,

			SPACE,
			BACK_SPACE,

			ESC,
			TAB,

			LEFT_SHIFT,
			RIGHT_SHIFT,

			LEFT_ATL,
			RIGHT_ALT,

			LEFT_CONTROL,
			RIGHT_CONTROL
		};
	};

	class Input
	{
	private:

		/** 
		* @brief estado del teclado
		*/
		const uint8_t * state;

		/**
		* @brief teclas de SDL
		*/
		static const uint8_t SDL_Scancodes[];

	public:

		Input();

	public:

		/**
		* @brief retorna la tecla pulsada
		*/
		const uint8_t getKey(Key::ScanCode key);

	};
}

#endif