
/**
* @file Window.hpp
* @author David Rogel Pernas
* @date 28/12/2017
* @class Window
* @brief Clase que encapsula la funcionalidad de una Window de SDL
*/

#ifndef LUBRE_WINDOW_HEADER
#define LUBRE_WINDOW_HEADER

union SDL_Event;
struct SDL_Window;

#include <cstdint>

namespace lubre
{
	class Window
	{
	public:
		
		// conjunto de variables que contienen parametros de SDL
		static const int opengl;
		static const int show;
		static const int hide;
		static const int borderless;
		static const int resizable;
		static const int minimized;
		static const int maximazed;
		static const int fullscreen;
		static const int fullscreen_desktop;
		
	private:

		/**
		* @brief Puntero a una ventana de SDL
		*/
		SDL_Window * window;

		/**
		* @brief ancho de la ventana
		*/
		int screenWidth;
		/**
		* @brief alto de la ventana
		*/
		int screenHeight;

	public:
		
		/**
		* @brief Creacion de una ventana
		* @param title -> titulo de la ventana
		* @param width -> ancho de la ventana
		* @param height -> alto de la ventana
		* @param flags -> comportamiento de la ventana
		*/
		Window(const char * title, int width, int height, int flags);
		/**
		* @brief Creacion de una ventana, tambien se le pasa su posicion X e Y
		* @param title -> titulo de la ventana
		* @param x -> posicion X de la ventana
		* @param y -> posicion Y de la ventana
		* @param width -> ancho de la ventana
		* @param height -> alto de la ventana
		* @param flags -> comportamiento de la ventana
		*/
		Window(const char * title, int x, int y, int width, int height, int flags);

		~Window();

	public:

		/**
		* @brief retorna el ancho de la ventana
		*/
		int getScreenWidth() const
		{
			return screenWidth;
		}

		/**
		* @brief retorna el alto de la ventana
		*/
		int getScreenHeight() const
		{
			return screenHeight;
		}

		/**
		* @brief retorna la ventana
		*/
		SDL_Window * getWindow() const
		{
			return window;
		}

		/**
		* @brief retorna el ID de la ventana
		*/
		int getWindowID();

	public:

		/**
		* @brief cambia el titulo de la ventan
		*/
		void setTitle(const char * nTitle);
		/**
		* @brief cambia el tama�o de la ventana, actualizando  el viewport
		*/
		void setSize(int nWidth, int nHeight);
		/**
		* @brief espera por los eventos de ventana
		*/
		int pollEvents(SDL_Event & e);

		/**
		* @brief SDL_GL_SetSwapInterval (devuelve 0 si todo va bien)
		* -> 0 para que la ventana se actualize inmediatamente
		* -> 1 para actualizar la ventana con el refresco del monitor (VSYNC)
		*/
		void setVSync(bool value);

	public:

		/**
		* @brief cambia los buffers de la ventana con opengl
		*/
		void swapBuffers();
		/**
		* @brief limpia la ventana con opengl
		*/
		void clearDisplay(float red = 0.2f, float green = 0.5f, float blue = 0.4f, float alpha = 1.0f);		
	};
}

#endif