
/**
* @file ModuleFactory.hpp
* @author David Rogel Pernas
* @date 16/1/2018
* @class ModuleFactory
* @brief Clase Factor�a para crear Modulos
*/

#ifndef LUBRE_MODULE_FACTORY_HEADER
#define LUBRE_MODULE_FACTORY_HEADER

#include <map>
#include <string>
#include <memory>

#include "Module.hpp"

namespace lubre
{
	class ModuleFactory
	{
	private:

		typedef std::string String;
		typedef std::shared_ptr<Module> SharedModule;

		/**
		* @brief puntero a funcion que devuelve un shared_ptr de Module  y recibe una escena
		*/
		typedef SharedModule (*pfnModuleFactory) (Scene *);

	private:

		/**
		* @brief mapa que guarda los punteros a funciones segun un key string
		*/
		std::map<String, pfnModuleFactory> mapModuleRegistry;

		/**
		* @brief puntero estatico a si mismo
		*/
		static ModuleFactory * instance;

	private:

		ModuleFactory();
		ModuleFactory(const ModuleFactory &) {}
		ModuleFactory & operator = (const ModuleFactory &) { return *this; }

	public:

		~ModuleFactory() 
		{ 
			mapModuleRegistry.clear(); 
		}

	public:

		/**
		* @brief se devuelve a si mismo, si no esta creado se crea
		* implementacion del patron singleton
		*/
		static ModuleFactory * getInstance() 
		{
			// if (!instance)
			// {
				// instance = new ModuleFactory;
			// }
			return (instance = new ModuleFactory);
		}

		/**
		* @brief registra un metodo que crea un componente
		* @param moduleName -> key para referirse al puntero a funcion
		* @param pfnFacatory -> puntero a funcion para crear un modulo
		*/
		void registated(const String & moduleName, pfnModuleFactory pfnFactory)
		{
			mapModuleRegistry[moduleName] = pfnFactory;
		}

		/**
		* @brief crea un modulo
		* @param moduleName -> key para referirse al puntero a funcion
		* @param scene -> escena padre con la que es creado el modulo
		*/
		SharedModule createModule(const String & moduleName, Scene * scene)
		{
			// si existe
			if (mapModuleRegistry.count(moduleName))
			{
				// invoca el metodo del puntero y retorna lo que devuelve ese puntero a ese metodo
				return mapModuleRegistry[moduleName](scene);
			}

			// si no devuelve un Modulo vacio
			return SharedModule();
		}
	};
}

#endif