
/**
* @file Entity.hpp
* @author David Rogel Pernas
* @date 28/12/2017
* @class Entity
* @brief Clase Entidad, sirve para guardar los componentes de una manera ordenada y poder acceder a ellos facilmente
* Tambien se puede acceder a la Escena 
*/

#ifndef LUBRE_ENTITY_HEADER
#define LUBRE_ENTITY_HEADER

#include <map>
#include <string>

#include "Component.hpp"

namespace lubre
{
	class Scene;

	class Entity
	{
	private:

		typedef std::shared_ptr<Component> SharedComponent;
		typedef std::string String;

	private:

		/**
		* @brief Mapa que guarda los Componentes
		*/
		std::map <String, SharedComponent> mapOfComponents;

		/**
		* @brief Escena a la que esta ligada la entidad
		*/
		Scene * parent;

	public:

		/**
		* @param scene -> escena a la que esta ligada la entidad
		*/
		Entity(Scene * scene);
		~Entity();

	public:
		
		/**
		* @brief Metodo para obtener un Componente por su nombre
		* @param name -> key del mapa
		*/
		SharedComponent getComponentByName(const String & name)
		{
			return mapOfComponents[name];
		}

		/**
		* @brief obtienes la escena a la que esta ligada la entidad
		*/
		Scene * getScene()
		{
			return parent;
		}
		/**
		* @brief obtienes la escena a la que esta ligada la entidad
		*/
		const Scene * getScene() const
		{
			return parent;
		}

	public:		

		/**
		* @brief a�ades un Componente a la entidad
		* @param name -> nombre del componente
		* @param component -> componente a a�adir
		*/
		void addComponent(const String & name, SharedComponent & component)
		{
			mapOfComponents[name] = component;
		}
	};
}

#endif