
/**
* @file Component.hpp
* @author David Rogel Pernas
* @date 28/12/2017
* @class Component
* @brief Clase abstracta que sirve para implementar los metodos basico de los componentes.
* Tambien guarda la Entidad padre
*/

#ifndef LUBRE_COMPONENT_HEADER
#define LUBRE_COMPONENT_HEADER

#include <memory>

#include <rapidxml.hpp>

namespace lubre
{
	class Entity;

	class Component
	{
	protected:

		typedef rapidxml::xml_node<> xmlNode;
		typedef rapidxml::xml_attribute<> xmlAttrib;

	protected:

		/**
		* Entidad Padre
		*/
		Entity * parent;

	public:

		/**
		* @param entity -> entidad padre
		*/
		Component(Entity * entity);

		virtual ~Component();

		/**
		* @brief Devuelve la entidad padre
		*/
		Entity * getParent()
		{
			return parent;
		}

	public:
		
		virtual void initialize() = 0;		
		virtual void run(float deltaTime) = 0;
		virtual void finalize() = 0;
		virtual bool parse(xmlNode * componentData) { return false; } // parsea los datos de cada componente
	};
}

#endif
