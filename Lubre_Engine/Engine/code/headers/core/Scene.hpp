
/**
* @file Scene.hpp
* @author David Rogel Pernas
* @date 28/12/2017
* @class Scene
* @brief La escena es la clase principal del juego,
* contiene la lista de entidad y modulos, 
* tiene un puntero al kernel, a la ventana y al dispatcher.
* Tambien inicializa el Kernel
*/

#ifndef LUBRE_SCENE_HEADER
#define LUBRE_SCENE_HEADER

#include "Entity.hpp"
#include "Module.hpp"
#include "Window.hpp"
#include "Kernel.hpp"
#include "Dispatcher.hpp"

namespace lubre
{
	class Scene
	{
	private:

		typedef std::string String;
		typedef std::shared_ptr<Entity> SharedEntity;
		typedef std::shared_ptr<Module> SharedModule;

		typedef rapidxml::xml_node<> xmlNode;
		typedef rapidxml::xml_attribute<> xmlAttrib;

	private:

		Kernel * kernel;
		Window * window;
		Dispatcher * dispatcher;

	private:

		/**
		* @brief mapa de las entidades
		*/
		std::map<String, SharedEntity> mapOfEntities;
		/**
		* @brief mapa de los modulos
		*/
		std::map<String, SharedModule> mapOfModules;

	public:

		/**
		* @brief Construye una escena, pasandole el archivo xml para parsearlo y un puntero a la ventana
		* @param sceneFilePath -> ruta al fichero xml
		* @param window -> puntero a la ventana
		*/
		Scene(const String & sceneFilePath, Window * window);

		~Scene()
		{			
			mapOfEntities.clear();
			mapOfModules.clear();
			kernel->finalize();

			delete kernel;
			delete window;
			delete dispatcher;
		}

	public:

		/**
		* @brief obtenemos la ventana 
		*/
		Window * getWindow() const
		{
			return window;
		}

		/**
		* @brief obtenemos el kernel
		*/
		Kernel * getKernel() const
		{
			return kernel;
		}

		/**
		* @brief obtenemos el dispatcher
		*/
		Dispatcher * getDispatcher() const
		{
			return dispatcher;
		}

		/**
		* @brief Obtenemos una entidad por el nombre
		* @param name -> nombre de la entidad
		*/
		SharedEntity getEntityByName(const String & name)
		{
			return mapOfEntities[name];
		}

		/**
		* @brief Obtenemos un modulo por el nombre
		* @param name -> nombre del modulo
		*/
		SharedModule getModuleByName(const String & name)
		{
			return mapOfModules[name];
		}

	public:
		
		/**
		* @brief ejecuta el kernel
		*/
		void execute()
		{
			kernel->execute();
		}

	private:

		/**
		* @brief carga la escena a traves de un xml
		* @param sceneFilepath -> ruta al archivo xml
		*/
		bool loadScene(const String & sceneFilePath);
		/**
		* @brief parsea la escena
		* @param sceneNode -> nodo xml de la escena
		*/
		bool parseScene(xmlNode * sceneNode);
		/**
		* @brief parsea las entidades
		* @param entitiesNode -> nodo xml a las entidades
		*/
		bool parseEntities(xmlNode * entitiesNode);
		/**
		* @brief parsea los componentes
		* @param componentsNode -> nodo xml de los componentes
		* @param entity -> entidad padre de los componentes
		*/
		bool parseComponents(xmlNode * componentsNode, Entity & entity);
	};
}

#endif
