
/**
* @file Kernel.hpp
* @author David Rogel Pernas
* @date 31/12/2017
* @class Kernel
* @brief Clase Kernel, encargada de guardar Tareas y hacerlas funcionar de una manera ordenada
*/

#ifndef LUBRE_KERNEL_HEADER
#define LUBRE_KERNEL_HEADER

#include <list>
#include <memory>
#include <chrono>

#include "Task.hpp"

namespace lubre
{
	class Kernel
	{
	private:

		typedef std::chrono::high_resolution_clock highClock;
		typedef std::chrono::time_point<highClock> highTimePoint;
		typedef std::chrono::duration<double> elapsed;

	private:

		/**
		* @brief guardamos el tiempo del ultimo frame
		*/
		highTimePoint lastTime;
		/**
		* @brief guardamos el tiempo de juego
		*/
		highTimePoint time;
		/**
		* @brief sirve para calcular el deltaTime
		*/
		elapsed deltaTime;

	private:

		/**
		* @brief lista de tareas
		*/
		std::list<Task *> taskList;		

	private:

		bool running;
		
	public:

		Kernel();

		~Kernel()
		{
			taskList.clear();
		}

	public:

		/**
		* @brief Paramos el kernel
		*/
		void stop()
		{
			running = false;
		}

	public:

		/**
		* @brief a�adimos una tarea al kernel
		* @param task -> tarea a a�adir
		*/
		void addTask(Task & task)
		{
			taskList.push_back(&task);
			taskList.sort(sortTasks);
		}

		/**
		* @brief quitamos una tarea al kernel
		* @param task -> tarea a quitar
		*/
		void removeTask(Task & task)
		{
			taskList.remove(&task);
		}

	public:

		/**
		* @brief metodo que inicia las tareas
		*/
		void initialize();
		/**
		* @brief metodo que ejecuta las tareas
		*/
		void execute();
		/**
		* @brief metodo que finaliza las tareas
		*/
		void finalize();

	private:

		/**
		* @brief prediado para poder ordena las tareas al a�adirlar
		*/
		static bool sortTasks(const Task * a, const Task * b)
		{
			return *a < *b;
		}
	};
}

#endif