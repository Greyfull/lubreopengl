
/**
* @file Module.hpp
* @author David Rogel Pernas
* @date 31/12/2017
* @class Module
* @brief Clase abstracta que sirve para implementar nuevos Modulos
*/

#ifndef LUBRE_MODULE_HEADER
#define LUBRE_MODULE_HEADER

#include <string>

#include "rapidxml.hpp"

#include "Task.hpp"

namespace lubre
{
	class Module
	{
	protected:

		typedef std::shared_ptr<Component> SharedComponent;
		typedef rapidxml::xml_node<> xmlNode;
		typedef std::string String;

	private:

		/**
		* @brief escena padre del modulo
		*/
		const Scene * parent;

	public:

		/**
		* @param scene -> escena padre
		*/
		Module(Scene * scene);
		virtual ~Module();

	public:

		virtual Task * getTask() { return nullptr; }
		virtual const Task * getTask() const { return nullptr; }
		virtual void createComponent(Entity * entity, xmlNode * componentData) = 0;
	};
}

#endif