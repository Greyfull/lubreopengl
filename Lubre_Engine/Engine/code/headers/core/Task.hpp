
/**
* @file Task.hpp
* @author David Rogel Pernas
* @date 31/12/2017
* @class Task
* @brief Clase abstracta para construir las Tareas
*/

#ifndef LUBRE_TASK_HEADER
#define LUBRE_TASK_HEADER

#include <list>

#include "Component.hpp"

namespace lubre
{
	class Scene;

	class Task
	{
	protected:

		typedef std::shared_ptr<Component> SharedComponent;

	protected:
		/**
		* @brief lista de componentes
		*/
		std::list<SharedComponent> components;

		/**
		* @brief prioridad de la entidad
		*/
		size_t priority;
		/**
		* @brief indica si la entidad es volatil o no
		*/
		bool finished;

	protected:

		/**
		* @brief puntero a la escena 
		*/
		Scene * scene;
		
	public:

		/**
		* @brief construye la Tarea
		* @param scene -> escena padre
		* @param priority -> prioridad de la task
		* @param finished -> volatilidad de la task
		*/
		Task(Scene * scene, size_t priority, bool finished = false);

		virtual ~Task();

	public:

		/**
		* @brief retorna la priorida de la tarea
		*/
		size_t getPriodity() const
		{
			return priority;
		}

		/**
		* @brief retorna si la tarea es volatil o no
		*/
		bool isFinished() const
		{
			return finished;
		}

		/**
		* @brief a�ades un componente a la tarea
		* @param component -> componente a a�adir
		*/
		void addComponent(SharedComponent component)
		{
			components.push_back(component);
		}

	public:

		virtual void run(float deltaTime) = 0;
		virtual void initialize() = 0;
		virtual void finalize() = 0;

	public:

		bool operator > (const Task & other) const
		{
			return this->priority > other.priority;
		}

		bool operator < (const Task & other) const
		{
			return !(*this > other);
		}

		bool operator >= (const Task & other) const
		{
			return this->priority >= other.priority;
		}

		bool operator <= (const Task & other) const
		{
			return !(*this >= other);
		}

		bool operator == (const Task & other) const
		{
			return this->priority == other.priority;
		}

		bool operator != (const Task & other) const
		{
			return !(*this == other);
		}
	};
}

#endif
