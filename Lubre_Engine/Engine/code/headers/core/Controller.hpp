
/**
* @file Controller.hpp
* @author David Rogel Pernas
* @date 3/2/2018
* @class Controller
* @brief Clase abstracta que sirve para implementar los metodos basico de los controllers.
*/

#ifndef LUBRE_CONTROLLER_HEADER
#define LUBRE_CONTROLLER_HEADER

#include "Entity.hpp"

namespace lubre
{
	class Controller
	{

	public:

		Controller() {}
		virtual ~Controller() {}

	public:

		virtual void initialize(Entity * entity) = 0;		
		virtual void update(Entity * entity, float deltaTime) = 0;
		virtual void finalize(Entity * entity) = 0;
	};
}

#endif