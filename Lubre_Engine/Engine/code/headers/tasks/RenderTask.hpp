
/**
* @file RenderTask.hpp
* @author David Rogel Pernas
* @date 1/1/2018
* @class RenderTask
* @brief Tarea de render donde se guaran los componentes de render.
* Tine un render_node donde se van a guardar todos los objetos a dibujar
*/

#ifndef LUBRE_RENDER_TASK_HEADER
#define LUBRE_RENDER_TASK_HEADER

#include <list>

#include <Render_Node.hpp>

#include "Task.hpp"

namespace lubre
{
	class RenderTask : public Task
	{
	private:

		typedef glt::Render_Node RenderNode;
		typedef std::shared_ptr<RenderNode> SharedRenderNode;

	private:

		/**
		* @brief nodo de render, 
		* donde se van a guardar los objetos dibujables
		*/
		SharedRenderNode renderNode;

	public:
		
		/**
		* @brief constructor de RenderTask
		* @param scene -> scena padre
		* @param renderNode -> nodo de renderizado
		* @param priority -> prioridad de la tarea, 1 por defecto
		* @param finished -> la tarea es volatil o no, false por defecto
		*/
		RenderTask
		(
			Scene * scene, 
			RenderNode * renderNode, 
			size_t priority = 1, 
			bool finished = false
		);

	public:

		/**
		* @brief retorna el renderNode
		*/
		SharedRenderNode getRenderNode()
		{
			return renderNode;
		}

	public:

		void initialize() override;
		void run(float deltaTime) override;		
		void finalize() override;
	};
}

#endif
