
/**
* @file EventTask.hpp
* @author David Rogel Pernas
* @date
* @class EventTask
* @brief Tarea de eventos donde se manejan los eventos de la ventana
*/

#ifndef LUBRE_EVENTS_TASK_HEADER
#define LUBRE_EVENTS_TASK_HEADER

#include "Task.hpp"

namespace lubre
{
	class EventsTask : public Task
	{
	public:

		/**
		* @brief Constructor de la tarea de eventos
		* @param scene -> escena padre
		* @param priority -> prioridad de la tarea, 100 por defecto
		* @param finished -> volatilidad de la tarea, false por defecto
		*/
		EventsTask(Scene * scene, int priority = 100, bool finished = false);

	public:

		void run(float deltaTime) override;
		void initialize() override;
		void finalize() override;
	};
}

#endif