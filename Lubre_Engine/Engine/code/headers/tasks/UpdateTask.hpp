
/**
* @file UpdateTask.hpp
* @author David Rogel Pernas
* @date 1/1/2018
* @class UpdateTask
* @brief Tarea de update que itera sobre todos los componentes
*/

#ifndef LUBRE_UPDATE_TASK_HEADER
#define LUBRE_UPDATE_TASK_HEADER

#include "Task.hpp"

namespace lubre
{
	class UpdateTask : public Task
	{
	public:

		/**
		* @brief constructor de UpdateTask
		* @param scene -> scena padre
		* @param priority -> prioridad de la tarea, 0 por defecto
		* @param finished -> la tarea es volatil o no, false por defecto
		*/
		UpdateTask(Scene * scene, size_t priority = 0, bool finished = false);

	public:

		void initialize() override;
		void run(float deltaTime) override;
		void finalize() override;
	};
}

#endif