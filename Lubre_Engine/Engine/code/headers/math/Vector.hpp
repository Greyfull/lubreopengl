/*
	Author: David Rogel Pernas
	Date: 1/1/2018
	Info: 
*/

#ifndef LUBRE_VECTOR_HEADER
#define LUBRE_VECTOR_HEADER

#include <cmath>

#include "Point.hpp"

namespace lubre
{
	template< typename TYPE, size_t size>
	class Vector : public Point<TYPE, size>
	{
	private:

		typedef TYPE Type;
		typedef Point<Type, size> ThePoint;

	public:

		Vector() : ThePoint() {}
		Vector(const Type(&arr)[size]) : ThePoint(arr) {}

		Vector(const Vector<Type, size> & other)
		{
			for (size_t i = 0; i < size; ++i)
			{
				data[i] = other.data[i];
			}
		}

	public:

		Type magnitude_sqr() const;
		Type magnitude() const;
		void normalize();	

		// producto escalar
		// para vectores unitarios (normalize)
		// 1 -> misma direccion | -1 -> direccion opuesta | 0 -> perpendicular
		Type dotProduct(const Vector<Type, size> & other);		
	};
	
	template<typename Type, size_t size>
	Type Vector<Type, size>::magnitude_sqr() const
	{
		Type result = Type(0);

		for (size_t i = 0; i < size; ++i)
		{
			result += data[i] * data[i];
		}

		return result;
	}

	template<typename Type, size_t size>
	Type Vector<Type, size>::magnitude() const
	{
		return Type(std::sqrt(magnitude_sqr()));
	}

	template<typename Type, size_t size>
	void Vector<Type, size>::normalize()
	{
		Type inverseNormalize = Type(1) / magnitude();

		for (size_t i = 0; i < size; ++i)
		{
			data[i] *= inverseNormalize;
		}
	}

	template<typename Type, size_t size>
	Type Vector<Type, size>::dotProduct(const Vector<Type, size> & other)
	{
		Type result = Type(0);

		for (size_t i = 0; i < size; ++i)
		{
			result += data[i] * other.data[i];
		}

		return result;
	}

	template<typename Type>
	class Vector<Type, 0>;


	typedef Vector<int, 2> vector2i;
	typedef Vector<int, 3> vector3i;
	typedef Vector<int, 4> vector4i;
	
	typedef Vector<unsigned int, 2> vector2ui;
	typedef Vector<unsigned int, 3> vector3ui;
	typedef Vector<unsigned int, 4> vector4ui;

	typedef Vector<float, 2> vector2f;
	typedef Vector<float, 3> vector3f;
	typedef Vector<float, 4> vector4f;

	typedef Vector<double, 2> vector2d;
	typedef Vector<double, 3> vector3d;
	typedef Vector<double, 4> vector4d;
}

#endif
