/*
	Author: David Rogel Pernas
	Date: 12/31/2017
	Info: 
*/


#ifndef LUBRE_POINT_HEADER
#define LUBRE_POINT_HEADER

#include <cstdint>
#include <initializer_list>

namespace lubre
{
	template<typename TYPE, size_t size>
	class Point
	{
	public:

		typedef TYPE Type;

	public:
		
		Type data[size];

	public:

		Point() {}
		Point(const Type(&arr)[size])
		{
			for (size_t i = 0; i < size; ++i)
			{
				data[i] = arr[i];
			}
		}

		Point(const Point<Type, size>& other)
		{
			for (size_t i = 0; i < size; ++i)
			{
				data[i] = other.data[i];
			}
		}

	public:

		void set(const Type(&arr)[size])
		{
			for (size_t i = 0; i < size; ++i)
			{
				data[i] = arr[i];
			}
		}

	public:

		// SUMA

		Point<Type, size> operator + (const Point<Type, size> & other) const
		{
			Point<Type, size> result(data);

			for (size_t i = 0; i < size; ++i)
			{
				result.data[i] += other.data[i];
			}

			return result;
		}

		Point<Type, size> & operator + (const Point<Type, size> & other)
		{
			for (size_t i = 0; i < size; ++i)
			{
				data[i] += other.data[i];
			}

			return *this;
		}

		Point<Type, size> & operator += (const Point<Type, size> & other)
		{
			return *this + other;
		}

		// RESTA

		void operator - ()
		{
			for (size_t i = 0; i < size; ++i)
			{
				data[i] = -data[i];
			}
		}

		Point<Type, size> operator - (const Point<Type, size> & other) const
		{
			Point<Type, size> result(data);

			for (size_t i = 0; i < size; ++i)
			{
				result.data[i] -= other.data[i];
			}

			return result;
		}

		Point<Type, size> & operator - (const Point<Type, size> & other)
		{
			for (size_t i = 0; i < size; ++i)
			{
				data[i] -= other.data[i];
			}

			return *this;
		}

		Point<Type, size> & operator -= (const Point<Type, size> & other)
		{
			return *this - other;
		}

		// MULTIPLICACION

		Point<Type, size> operator * (const Point<Type, size> & other) const
		{
			Point<Type, size> result(data);

			for (size_t i = 0; i < size; ++i)
			{
				result.data[i] *= other.data[i];
			}

			return result;
		}

		Point<Type, size> & operator * (Type k)
		{
			for (size_t i = 0; i < size; ++i)
			{
				data[i] *= k;
			}

			return *this;
		}

		Point<Type, size> & operator * (const Point<Type, size> & other)
		{
			for (size_t i = 0; i < size; ++i)
			{
				data[i] *= other.data[i];
			}

			return *this;
		}

		Point<Type, size> & operator *= (const Point<Type, size> & other)
		{
			return *this * other;
		}

		Point<Type, size> & operator *= (Type k)
		{
			return *this * k;
		}

		// ASIGNACION

		const Point<Type, size> & operator = (const Point<Type, size> & other)
		{
			for (size_t i = 0; i < size; ++i)
			{
				data[i] = other.data[i];
			}

			return *this;
		}

		// COMPARACION
		
		bool operator == (const Point<Type, size> & other)
		{
			bool result = false;

			for (size_t i = 0; i < size; ++i)
			{
				result = data[i] == other.data[i];
			}

			return result;
		}

		bool operator != (const Point<Type, size> & other)
		{
			return !(*this == other);
		}
		
	};

	template<typename Type, size_t size>
	Point<Type, size> operator * (Type k, const Point<Type, size> & other)
	{
		Point<Type, size> result;

		for (size_t i = 0; i < size; ++i)
		{
			result.data[i] = other.data[i] * k;
		}

		return result;
	}
	
	template<typename TYPE>
	class Point<TYPE, 0>;

	typedef Point<int, 2> point2i;
	typedef Point<int, 3> point3i;
	typedef Point<int, 4> point4i;

	typedef Point<unsigned int, 2> point2ui;
	typedef Point<unsigned int, 3> point3ui;
	typedef Point<unsigned int, 4> point4ui;

	typedef Point<float, 2> point2f;
	typedef Point<float, 3> point3f;
	typedef Point<float, 4> point4f;

	typedef Point<double, 2> point2d;
	typedef Point<double, 3> point3d;
	typedef Point<double, 4> point4d;
}

#endif