/*
	Author: David Rogel Pernas
	Date: 1/1/2018
	Info:
*/

#ifndef LUBRE_MATRIX_HEADER
#define LUBRE_MATRIX_HEADER

#include <cstdint>

namespace lubre
{
	template <typename TYPE, size_t M = 3, size_t N = 3>
	class Matrix
	{
	private:

		typedef TYPE Type;

	public:

		Type data[M * N];

		const size_t rows = M;
		const size_t columns = N;

	public:

		Matrix() {}
		Matrix(const Type(&arr)[M * N]) : dimension(Type(M * N))
		{
			size_t dimension = M * N;
			for (size_t i = 0; i < dimension; ++i)
			{
				data[i] = arr[i];
			}
		}

		Matrix(const Matrix<Type, M, N> & other)
		{
			size_t dimension = M * N;
			for (size_t i = 0; i < dimension; ++i)
			{
				data[i] = other.data[i];
			}
		}

	public:

		static Matrix<Type, M, N> identity()
		{
			Matrix<Type, M, N> result;
			size_t dimension = M * N;
			for (size_t i = 0; i < dimension; ++i) result.data[i] = Type(0);

			for (size_t offset = 0; offset < dimension; offset += N + 1)
			{
				result.data[offset] = Type(1);
			}

			return result;
		}

	public:

		// MULTIPLICACION

		Matrix<Type, M, N> & operator * (const Matrix<Type, M, N> & other)
		{
			size_t dimension = M * N;

			for (size_t i = 0; i < dimension; ++i)
			{
				Type result = Type(0);

			}

			return *this;
		}

		// ASIGNACION
		
		Matrix<Type, M, N> & operator = (const Matrix<Type, M, N> & other)
		{
			size_t dimension = M * N;
			for (size_t i = 0; i < dimension; ++i)
			{
				data[i] = other.data[i];
			}

			return *this;
		}
	};
}

#endif