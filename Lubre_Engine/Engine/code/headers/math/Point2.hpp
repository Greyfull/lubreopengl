/*
	Author: David Rogel Pernas
	Date: 12/28/2017
	Info: 
*/

#ifndef LUBRE_POINT2_HEADER
#define LUBRE_POINT2_HEADER

#include <cstdint>

namespace lubre
{
	//namespace math
	//{
	template < typename TYPE >
	class Point2
	{
	private:

		typedef TYPE Type;

	public:

		Type x, y;

	public:

		Point2() : x(Type(0)), y(Type(0)) {}
		Point2(Type xx, Type yy) : x(xx), y(yy) {}

		Point2(const Point2 & other) : x(other.x), y(other.y) {}

	public:

		void set(Type xx = Type(0), Type yy = Type(0))
		{
			x = xx;
			y = yy;
		}

	public:

		// SUMA

		Point2<Type> operator + (const Point2<Type> & other) const
		{
			return Point2<Type>(x + other.x, y + other.y);
		}

		Point2<Type> & operator + (const Point2<Type> & other)
		{
			x += other.x;
			y += other.y;

			return *this;
		}

		Point2<Type> & operator += (const Point2<Type> & other)
		{
			return *this + other;
		}

		// RESTA

		void operator - ()
		{
			x = -x;
			y = -y;
		}

		Point2<Type> operator - (const Point2<Type> & other) const
		{
			return Point2<Type>(x - other.x, y - other.y);
		}

		Point2<Type> & operator - (const Point2<Type> & other)
		{
			x -= other.x;
			y -= other.y;

			return *this;
		}

		Point2<Type> & operator -= (const Point2<Type> & other)
		{
			return *this - other;
		}

		// MULTIPLICACION

		Point2<Type> operator * (const Point2<Type> & other) const
		{
			return Point2<Type>(x * other.x, y * other.y);
		}

		Point2<Type> & operator * (const Point2<Type> & other)
		{
			x *= other.x;
			y *= other.y;

			return *this;
		}

		Point2<Type> & operator *= (const Point2<Type> & other)
		{
			return *this *= other;
		}

		// ASIGNACION

		Point2<Type> & operator = (const Point2<Type> & other)
		{
			x = other.x;
			y = other.y;

			return *this;
		}

		// COMPARACION

		bool operator == (const Point2<Type> & other)
		{
			return (x == other.x && y == other.y);
		}

		bool operator != (const Point2<Type> & other)
		{
			return !(*this == other);
		}
	};

	/*typedef Point2<int> point2i;
	typedef Point2<unsigned int> point2ui;

	typedef Point2<float> point2f;
	typedef Point2<double> point2d;

	typedef Point2<uint8_t> point2ui_8;
	typedef Point2<uint16_t> point2ui_16;
	typedef Point2<uint32_t> point2ui_32;
	typedef Point2<uint64_t> point2ui_64;

	typedef Point2<int8_t> point2i_8;
	typedef Point2<int16_t> point2i_16;
	typedef Point2<int32_t> point2i_32;
	typedef Point2<int64_t> point2i_64;*/

	//}
}

#endif
