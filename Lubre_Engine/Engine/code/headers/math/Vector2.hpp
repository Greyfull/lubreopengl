/*
	Author: David Rogel Pernas
	Date: 12/28/2017
	Info: 
*/

#ifndef LUBRE_VECTOR2_HEADER
#define LUBRE_VECTOR2_HEADER

#include <cmath>

#include "Point2.hpp"

namespace lubre
{
	//namespace math
	//{

	template < typename TYPE >
	class Vector2 : public Point2<TYPE>
	{
	private:

		typedef TYPE Type;

	public:

		Vector2() : Point2<Type>() {}
		Vector2(Type xx, Type yy) : Point2<Type>(xx, yy) {}

		Vector2(const Vector2<Type> & other) : x(other.x), y(other.y) {}

	public:

		Type magnitude_sqr()
		{
			return x * x + y * y;
		}

		Type magnitude()
		{
			return Type(std::sqrt(magnitude_sqr()));
		}

		// producto escalar
		// para vectores unitarios
		// 1 -> misma direccion | -1 -> direccion opuesta | 0 -> perpendicular
		Type dot_product(const Vector2<Type> & other)
		{
			return x * other.x + y * other.y;
		}

		void normalize()
		{
			Type inverse_normalize = Type(1) / magnitude();

			x *= inverse_normalize;
			y *= inverse_normalize;
		}

		Vector2<Type> get_normalized_vector()
		{
			Vector2<Type> result(x, y);

			result.normalize();

			return result;
		}

	};

	/*typedef Vector2<int> vector2i;
	typedef Vector2<unsigned int> vector2ui;

	typedef Vector2<float> vector2f;
	typedef Vector2<double> vector2d;*/

	//}
}

#endif