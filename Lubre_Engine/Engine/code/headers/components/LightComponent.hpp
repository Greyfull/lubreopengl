
/**
* @file LightComponent.hpp
* @author David Rogel Pernas
* @date 28/1/2018
* @class LightComponent
* @brief Clase que encapsula la clase light del repositorio openglToolkit, inicializandola.
* Tiene un metodo para poder acceder a la luz
*/

#ifndef LUBRE_LIGHT_COMPONENT_HEADER
#define LUBRE_LIGHT_COMPONENT_HEADER

#include <Light.hpp>

#include "Component.hpp"

namespace lubre
{
	class LightComponent : public Component
	{
	private:

		typedef glt::Light Light;
		typedef std::shared_ptr<Light> SharedLight;

	private:

		/**
		* @brief shared_ptr a Light
		*/
		SharedLight light;

	public:

		/**
		* @param entity -> entidad padre
		*/
		LightComponent(Entity * entity);

	public:

		/**
		* @brief Devuelve la Light
		*/
		SharedLight getLight() const
		{
			return light;
		}

		/**
		* @brief Cambia el color de la luz
		* @warning los colores deben estar en rango [0,1]
		* @code
		* changeLightColor(0.2f, 0.1f, 0.0f);
		* @endcode
		*/
		void changeLightColor(float red = 1.f, float green = 1.f, float blue = 1.f);

	public:

		void initialize() override;
		void run(float deltaTime) override;
		void finalize() override;
		bool parse(xmlNode * node) override;
	};
}

#endif