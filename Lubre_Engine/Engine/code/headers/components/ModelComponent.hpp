
/**
* @file ModelComponent.hpp
* @author David Rogel Pernas
* @date 28/1/2018
* @class ModelComponent
* @brief Clase que encapsula la clase Model del repositorio openglToolkit, inicializandola.
* Tiene un metodo para poder acceder al modelo
*/

#ifndef LUBRE_MODEL_COMPONENT_HEADER
#define LUBRE_MODEL_COMPONENT_HEADER

#include <Model_Obj.hpp>
#include <Material.hpp>

#include "Component.hpp"

namespace lubre
{
	class ModelComponent : public Component
	{
	private:

		typedef std::string String;

		typedef glt::Model_Obj ModelObj;
		typedef std::shared_ptr<glt::Model> SharedModel;

	private:

		/**
		* @brief shared_ptr a Model
		*/
		SharedModel model;

	public:

		/**
		* @param entity -> entidad padre
		*/
		ModelComponent(Entity * entity);

		/**
		* @param entity -> entidad padre
		* @param filePath -> ruta al archivo del modelo
		*/
		ModelComponent(Entity * entity, const std::string & filePath);

	public:

		/**
		* @brief Devuelve el Model
		*/
		SharedModel getModel()
		{
			return model;
		}

	public:

		void initialize() override;
		void run(float deltaTime) override;
		void finalize() override;
		bool parse(xmlNode * componentData) override;
	};
}

#endif