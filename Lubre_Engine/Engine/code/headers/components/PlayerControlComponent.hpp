
/**
* @file PlayerControlComponent.hpp
* @author David Rogel Pernas
* @date 4/2/2018
* @class PlayerControlComponent
* @brief Clase Componente del PlayerController
* la cual llama a los metodos basicos del PlayerController para que funcione
*/

#ifndef LUBRE_PLAYER_CONTROL_COMPONENT_HEADER
#define LUBRE_PLAYER_CONTROL_COMPONENT_HEADER

#include "ControlComponent.hpp"
#include "PlayerController.hpp"

namespace lubre
{	
	class PlayerControlComponent : public ControlComponent, public Dispatcher::Listener
	{
	public:

		PlayerControlComponent(Entity * entity, Controller * control);

		~PlayerControlComponent();

	public:
		
		/**
		* @brief Metodo que maneja los mensajes enviados por el jugador a traves del input.
		* Este metodo se llama desde el dispatcher
		* @param mess -> Mensaje que se va a usar
		*/
		void handle(Message & mess) override;

	public:

		void initialize() override
		{
			this->control->initialize(parent);
		}

		void run(float deltaTime) override
		{
			this->control->update(parent, deltaTime);
		}

		void finalize() override
		{
			this->control->finalize(parent);
		}

		bool parse(xmlNode * node) override
		{
			return false;
		}
	};
}

#endif
