/**
* @file ControlComponent.hpp
* @author David Rogel Pernas
* @date 3/2/2018
* @class ControlComponent
* @brief Clase abstracta que guarda un Controller 
* y hereda de Component los metodos basicos a implementar
*/


#ifndef LUBRE_CONTROL_COMPONENT_HEADER
#define LUBRE_CONTROL_COMPONENT_HEADER

#include "TransformComponent.hpp"
#include <Node.hpp>

#include "Component.hpp"
#include "Controller.hpp"

#include "Dispatcher.hpp"
#include "Messages.hpp"

namespace lubre
{
	class ControlComponent : public Component
	{
	protected:

		/**
		* @brief Puntero a Controller
		*/
		Controller * control;

	public:

		ControlComponent(Entity * entity, Controller * control)
			: Component(entity), control(control)
		{
		}

		virtual ~ControlComponent()
		{
		}
	};
}

#endif