
/**
* @file CameraComponent.hpp
* @author David Rogel Pernas
* @date 27/1/2018
* @class CameraComponent
* @brief Clase que encapsula la clase c�mara del repositorio openglToolkit, inicializandola.
* Tiene un metodo para poder acceder a la c�mara
*/

#ifndef LUBRE_CAMERA_COMPONENT_HEADER
#define LUBRE_CAMERA_COMPONENT_HEADER

#include "Component.hpp"
#include <Camera.hpp>

namespace lubre
{
	class CameraComponent : public Component
	{
	private:

		typedef glt::Camera Camera;
		typedef std::shared_ptr<Camera> SharedCamera;

	private:
		
		/**
		* @brief shared_ptr a Camera
		*/
		SharedCamera camera;

	public:

		/*
		* @brief Construlle una c�mara con unos parametros por defecto
		* @param entity -> entidad padre
		* @param fov -> Field of View de la camara = 90.f por defecto
		* @param nearPlane -> Near Plane de la camara = 0.3f por defecto
		* @param farPlane -> Far Plane de la camara = 1000.f por defecto
		* @param aspectRatio -> Aspect Ratio de la camara = 1.f por defecto
		*/
		CameraComponent
		(
			Entity * entity, 
			float fov = 90.f, 
			float nearPlane = 0.3f, 
			float farPlane = 1000.f, 
			float aspectRatio = 1.f
		);

	public:

		/**
		* @brief Devuelve la c�mara
		*/
		SharedCamera getCamera() const
		{
			return camera;
		}

	public:

		void initialize() override;
		void run(float deltaTime) override;
		void finalize() override;
		bool parse(xmlNode * node) override;
	};
}

#endif