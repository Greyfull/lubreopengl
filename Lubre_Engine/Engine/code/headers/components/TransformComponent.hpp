
/**
* @file TransformComponent.hpp
* @author David Rogel Pernas
* @date 16/1/2018
* @class TransformComponent
* @brief Clase que encapsula la clase Node del repositorio openglToolkit.
* Esta clase sive para acceder a la posicion del objeto
*/

#ifndef LUBRE_TRANSFORM_COMPONENT_HEADER
#define LUBRE_TRANSFORM_COMPONENT_HEADER

#include <Node.hpp>

#include "Component.hpp"

namespace lubre
{
	class TransformComponent : public Component
	{
	private:
		
		typedef glt::Node Node;
		typedef std::shared_ptr<Node> SharedNode;
		
	private:

		/**
		* @brief shared_ptr a Node
		*/
		SharedNode myTransform;

	private:

		/**
		* @brief posicion del objeto
		*/
		glm::vec3 position;

		/**
		* @brief rotacion en el eje X del objeto
		*/
		float rotationX;
		/**
		* @brief rotacion en el eje Y del objeto
		*/
		float rotationY;
		/**
		* @brief rotacion en el eje Z del objeto
		*/
		float rotationZ;
		
		/**
		* @brief escala del objeto
		*/
		float scale;

	public:	

		/**
		* @param entity -> entidad padre
		*/
		TransformComponent(Entity * entity);

	public:

		/**
		* @brief devuelve la position del objeto		
		*/
		glm::vec3 getPosition() const
		{
			return position;
		}

		/**
		* @brief asigna el Node del Transform
		* @param compNode -> componente con un Node
		*/
		void setComponentTrasnform(SharedNode compNode)
		{
			myTransform = compNode;
		}

	public:

		/**
		* @brief aplica las posiciones rotaciones y escalas
		*/
		void applyPositionRotationAndScale();
		
	public:

		/**
		* @brief metodo para mover el objeto
		* @param pos -> posicion que se va a incrementar
		*/
		void move(glm::vec3 pos)
		{
			position += pos;
			myTransform->translate(pos);
		}

	public:
		
		void initialize() override;
		void run(float deltaTime) override;
		void finalize() override;
		bool parse(xmlNode * node) override;
	};
}

#endif