
/**
* @file UpdateModule.hpp
* @author David Rogel Pernas
* @date 17/1/2018
* @class UpdateModule
* @brief clase que hereda de Modulo y crea un UpdateModule
* que contiene una UpdateTask y crea los Componentes correspondientes
*/

#ifndef LUBRE_UPDATE_MODULE_HEADER
#define LUBRE_UPDATE_MODULE_HEADER

#include "Module.hpp"
#include "UpdateTask.hpp"

namespace lubre
{
	class UpdateModule : public Module
	{
	private:

		typedef std::shared_ptr<Module> SharedModule;

	private:

		/**
		* @brief tarea de update
		*/
		UpdateTask updateTask;

	public:

		/**
		* @brief constructor de RenderModule
		* @param scene -> escena padre
		*/
		UpdateModule(Scene * scene) : Module(scene), updateTask(scene)
		{
		}

		~UpdateModule() {}

	public:

		/**
		* @brief retornas la tarea de render
		*/
		Task * getTask() override
		{
			return &updateTask;
		}

		/**
		* @brief retornas la tarea de render
		*/
		const Task * getTask() const override
		{
			return &updateTask;
		}

	public:
		
		/**
		* @brief metodo estatico para crear el modulo
		* @param scene -> escena padre
		*/
		static SharedModule create(Scene * scene)
		{
			return SharedModule(new UpdateModule(scene));
		}

	public:

		/**
		* @brief crea un componente
		* @param entity -> entidad padre
		* @param componentData -> nodo xml del componente
		*/
		void createComponent(Entity * entity, xmlNode * componentData) override;
	};
}

#endif
