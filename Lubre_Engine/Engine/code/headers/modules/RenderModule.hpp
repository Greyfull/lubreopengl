
/**
* @file RenderModule.hpp
* @author David Rogel Pernas
* @date 16/1/2018
* @class RenderModule
* @brief clase que hereda de Modulo y crea un RenderModule
* que contiene una RenderTask y crea los Componentes correspondientes
*/

#ifndef LUBRE_RENDER_MODULE_HEADER
#define LUBRE_RENDER_MODULE_HEADER

#include "Module.hpp"
#include "RenderTask.hpp"

namespace lubre
{
	class RenderModule : public Module
	{
	private:

		typedef glt::Render_Node RenderNode;
		typedef std::shared_ptr<Module> SharedModule;

		typedef rapidxml::xml_attribute<> xmlAttrib;

	private:

		/**
		* @brief tarea de render
		*/
		RenderTask renderTask;
		
	public:

		/**
		* @brief constructor de RenderModule
		* @param scene -> escena padre
		*/
		RenderModule(Scene * scene) : Module(scene), renderTask(scene, new RenderNode)
		{
		}

		~RenderModule() {}

	public:

		/**
		* @brief retornas la tarea de render
		*/
		Task * getTask() override
		{
			return &renderTask;
		}

		/**
		* @brief retornas la tarea de render
		*/
		const Task * getTask() const override
		{
			return &renderTask;
		}

	public:

		/**
		* @brief metodo estatico para crear el modulo
		* @param scene -> escena padre
		*/
		static SharedModule create(Scene * scene)
		{
			return SharedModule(new RenderModule(scene));
		}

	public:

		/**
		* @brief crea un componente
		* @param entity -> entidad padre
		* @param componentData -> nodo xml del componente
		*/
		void createComponent(Entity * entity, xmlNode * componentData) override;
	};
}

#endif