// assimp -> para cargar objs, fbxs, etc
// yobi3d -> para buscar modelos

#include <Window.hpp>
#include <Scene.hpp>

using namespace lubre;

extern "C"
int SDL_main(int argc, char * argv[])
{
	// creamos una ventana reescalable
	Window window("test", 800, 600, Window::show | Window::resizable);

	// activamos la sincronizacion vertical
	window.setVSync(true);
	
	// creamos la escena
	Scene * scene = new Scene
	(
		(argv[1] == nullptr)
		? 
		"../../resources/scene.xml"
		: 
		argv[1],
		&window
	);

	// ejecutamos la escena
	scene->execute();
	
	return 0;
}
